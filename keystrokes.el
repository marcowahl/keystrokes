;;; keystrokes.el --- Display keystrokes within Emacs   -*- lexical-binding: t; -*-

;; Copyright (C) 2018-2020

;; Author: Marco Wahl <marcowahlsoft@gmail.com>
;; Keywords: help
;; Version: 0.0.1
;; URL: https://gitlab.com/marcowahl/keystrokes

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This program gives a way to see each keystroke at the time of
;; typing.

;; # Activate #

;; Activate the functionality with

;;     M-x keystrokes-mode

;; Open buffer "*keystrokes" in an extra frame (C-x 5 b *keystrokes).

;; Enjoy.

;; # Deactivate #

;; Deactivate the funcionality with

;;     M-x keystrokes-mode

;; Alternatives:

;; - screenkey (external program)


;;; Code:

(define-minor-mode keystrokes-mode
  "Display typed keystorkes in buffer *keystrokes."
  :lighter " Keystrokes"
  :global t
  (if keystrokes-mode
      (add-hook 'pre-command-hook #'keystrokes-display)
    (remove-hook 'pre-command-hook #'keystrokes-display)))

(defun keystrokes-display ()
  "Display keystorkes in buffer *keystrokes.

This function is mostly a copy of `view-lossage'."
  (let ((buf (get-buffer-create "*keystrokes")))
    (with-current-buffer buf
      (erase-buffer)
      (princ " " (current-buffer))
      (princ (mapconcat (lambda (key)
			  (cond
			   ((and (consp key) (null (car key)))
			    (format "\t\t;; %s\n" (if (symbolp (cdr key)) (cdr key)
					        "anonymous-command")))
			   ((or (integerp key) (symbolp key) (listp key))
			    (single-key-description key))
			   (t
			    (prin1-to-string key nil))))
		        (recent-keys 'include-cmds)
		        " ")
             (current-buffer))
      (reverse-region (point-min) (point-max)))))

(provide 'keystrokes)
;;; keystrokes.el ends here
