keystroke.el
============

This program gives a way to see each keystroke at the time of
typing.

keystroke.el might be helpful when presenting about emacs itself to make the keystrokes transparent.

# Activate #

Activate the functionality with

    M-x keystrokes-mode

Open buffer "*keystrokes" in an extra frame (C-x 5 b *keystrokes).

Enjoy.

# Deactivate #

Deactivate the funcionality with

    M-x keystrokes-mode

# Similar stuff #

- There is variable 'echo-keystrokes' which can be customized to display certain keystrokes quasi immediately.

- There is function 'view-lossage' which shows a bunch of keystrokes.  'view-lossage' is the main shoulder on which keystroke.el stands on.

- (open-dribble-file "DRIBBLEFILE") writes keystrokes to the file DRIBBLEFILE.  The format looks merely suitable for machines.

- MELPA has command-log-mode which most likely is much more sophisticated than keystroke.el.

- Newcomer in 2020: https://github.com/chuntaro/emacs-keypression.

- See also https://emacs.stackexchange.com/questions/122/is-there-a-mode-for-viewing-keystroke-lossage-in-realtime

- There are external programs to display the keystrokes.  E.g. screenkey https://github.com/wavexx/screenkey.
